package com.almis.ade.api.bean.input;

import com.almis.ade.api.bean.component.Element;
import lombok.Data;
import lombok.experimental.Accessors;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.constant.PageType;

/**
 * PrintBean class
 * POJO with all report elements
 */
@Data
@Accessors(chain = true)
public class PrintBean {

  private Element title;
  private Element pageHeader;
  private Element columnHeader;
  private Element detail;
  private Element columnFooter;
  private Element pageFooter;
  private Element summary;
  private Element lastPageFooter;
  private Element noData;
  private Element background;
  private String reportName;
  private String author;
  private PageOrientation orientation;
  private int pageMargin = 30;
  private PageType pageType = PageType.A4;
}