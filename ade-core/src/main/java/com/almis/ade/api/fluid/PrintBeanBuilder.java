package com.almis.ade.api.fluid;

import com.almis.ade.api.fluid.engine.generic.JasperEngineBuilder;
import com.almis.ade.config.AdeConfigProperties;

/**
 *
 * @author dfuentes
 */
public class PrintBeanBuilder {

  private final AdeConfigProperties adeConfigProperties;

  public PrintBeanBuilder(AdeConfigProperties adeConfigProperties) {
    this.adeConfigProperties = adeConfigProperties;
  }

  /**
   * Print bean with Jasper Engine
   *
   * @return JasperEngineBuilderService
   */
  public JasperEngineBuilder withJasper(){
    return new JasperEngineBuilder(adeConfigProperties);
  }
}
