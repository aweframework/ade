package com.almis.ade.api.engine.jasper.generation.builder.component.element;

import com.almis.ade.api.bean.component.Barcode;
import jakarta.validation.constraints.NotNull;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;

import java.util.Objects;

/**
 *
 * @author dfuentes
 */
public class BarcodeBuilder extends ElementBuilder<Barcode, ComponentBuilder> {

  /**
   *
   * @param element barcode element
   * @param jasperReportBuilder jasper report builder
   * @return ComponentBuilder
   */
  @Override
  public ComponentBuilder build(@NotNull Barcode element, JasperReportBuilder jasperReportBuilder) {
    return Objects.requireNonNull(element.getBarcodeType().getBarcode(element.getCode())).setStyle(element.getStyle());
  }
}
