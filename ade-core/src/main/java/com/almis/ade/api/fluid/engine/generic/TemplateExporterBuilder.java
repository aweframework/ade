package com.almis.ade.api.fluid.engine.generic;

import com.almis.ade.api.bean.style.StyleTemplate;
import com.almis.ade.api.util.JasperFileUtil;
import com.almis.ade.config.AdeConfigProperties;
import lombok.extern.slf4j.Slf4j;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.jasper.builder.export.*;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static net.sf.dynamicreports.report.builder.DynamicReports.exp;
import static net.sf.dynamicreports.report.builder.DynamicReports.export;

/**
 * Template exporter builder
 */
@Slf4j
public class TemplateExporterBuilder {
  
  private static final String EXTENSION_PATTERN = "{extension}";
  private String defaultName;
  private String defaultPath;
  private JasperReportBuilder reportBuilder;
  private Map<String, Object> data;
  private JRDataSource dataSource;

  /**
   * TemplateExporterBuilder constructor
   * @param adeConfigProperties ADE config properties
   */
  public TemplateExporterBuilder(AdeConfigProperties adeConfigProperties) {
    this.defaultName = adeConfigProperties.getDocument().getDefaultName();
    this.defaultPath = adeConfigProperties.getDocument().getDefaultPath();
  }

  /**
   * Initialize class
   *
   * @param reportBuilder report builder
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder initialize(JasperReportBuilder reportBuilder) {
    this.reportBuilder = reportBuilder;
    return this;
  }

  /**
   * set current document Name
   *
   * @param name document name
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder withName(String name) {
    this.defaultName = name;
    return this;
  }

  /**
   * Set current document save path
   *
   * @param path document path
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder withPath(String path) {
    this.defaultPath = path.endsWith(File.separator) ? path : path + File.separator;
    return this;
  }

  /**
   * Set data for report
   *
   * @param key   data key
   * @param value value data
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder withData(String key, Object value) {
    if (data == null) {
      data = new HashMap<>();
    }
    data.put(key, value);
    return this;
  }

  /**
   * Set data for report
   *
   * @param data report data
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder withData(Map<String, Object> data) {
    this.data = data;
    return this;
  }

  /**
   * Set datasource for report
   *
   * @param dataSource report data source
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder withDataSource(JRDataSource dataSource) {
    this.dataSource = dataSource;
    return this;
  }

  /**
   * Get data for report
   *
   * @return data map
   */
  public Map<String, Object> getData() {
    return data;
  }

  /**
   * Get datasource for report
   *
   * @return JRDataSource
   */
  public JRDataSource getDataSource() {
    return dataSource;
  }

  /**
   * Retrieve template path
   *
   * @return Template path
   */
  public String getTemplatePath() {
    this.defaultPath = defaultPath.endsWith(File.separator) ? defaultPath : defaultPath + File.separator;

    if (Paths.get(defaultPath).toFile().mkdirs()) {
      //Throw exception, couldn't create directories
    }

    return defaultPath + defaultName + "." + EXTENSION_PATTERN;
  }

  /**
   * Export to JRXML
   *
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder toJRXML() {
    if (!Paths.get(getTemplatePath().replace(EXTENSION_PATTERN, "jrxml")).toFile().exists()) {
      // Save the template to a File
      File jrxmlFile = new File(getTemplatePath().replace(EXTENSION_PATTERN, "jrxml"));
      try (OutputStream jrxmlFileStream = Files.newOutputStream(jrxmlFile.toPath())) {
        // Export to jasperPrint file
        JasperFileUtil.exportReportToJRXMLFile(reportBuilder, jrxmlFileStream);
        log.info("JRXML file generated");
      } catch (IOException | DRException exc) {
        log.error("Error exporting to JRXML - {}", getTemplatePath(), exc);
      }
    }
    return this;
  }

  /**
   * Export to JRPXML
   *
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder toJRPXML() {
    if (!Paths.get(getTemplatePath().replace(EXTENSION_PATTERN, "jrpxml")).toFile().exists()) {
      // Save the template to a File
      File jprxmlFile = new File(getTemplatePath().replace(EXTENSION_PATTERN, "jrpxml"));
      try (OutputStream jrpxmlFileStream = Files.newOutputStream(jprxmlFile.toPath())) {
        // Export to jasperPrint file
        JasperFileUtil.exportReportToJRPXMLFile(reportBuilder, jrpxmlFileStream);
        log.info("JRPXML file generated");
      } catch (IOException | JRException | DRException exc) {
        log.error("Error exporting to JRPXML - {}", getTemplatePath(), exc);
      }
    }
    return this;
  }

  /**
   * Export to Jasper
   *
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder toJasper() {
    if (!Paths.get(getTemplatePath().replace(EXTENSION_PATTERN, "jasper")).toFile().exists()) {
      // Save the template to a File
      File jasperFile = new File(getTemplatePath().replace(EXTENSION_PATTERN, "jasper"));
      try (OutputStream jasperFileStream = Files.newOutputStream(jasperFile.toPath())) {
        // Export to jasperPrint file
        JasperFileUtil.exportReportToJasperFile(reportBuilder, jasperFileStream);
        log.info("Jasper file generated");
      } catch (IOException | JRException | DRException exc) {
        log.error("Error exporting to Jasper - {}", getTemplatePath(), exc);
      }
    }
    return this;
  }

  /**
   * Export to PDF
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toPDF() throws DRException {
    JasperPdfExporterBuilder pdfExporterBuilder = export.pdfExporter(getTemplatePath().replace(EXTENSION_PATTERN, "pdf"));
    reportBuilder.toPdf(pdfExporterBuilder);
    return this;
  }

  /**
   * Export to XML
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toXML() throws DRException {
    JasperXmlExporterBuilder xmlExporterBuilder = export.xmlExporter(getTemplatePath().replace(EXTENSION_PATTERN, "xml"));
    reportBuilder.toXml(xmlExporterBuilder);
    return this;
  }

  /**
   * Export to HTML
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toHTML() throws DRException {
    JasperHtmlExporterBuilder htmlExporterBuilder = export.htmlExporter(getTemplatePath().replace(EXTENSION_PATTERN, "html"))
      .setAccessibleHtml(true);
    reportBuilder.toHtml(htmlExporterBuilder);
    return this;
  }

  /**
   * Export to CSV
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toCsv() throws DRException {
    JasperCsvExporterBuilder csvExporter = export.csvExporter(getTemplatePath().replace(EXTENSION_PATTERN, "csv"));
    reportBuilder.toCsv(csvExporter);
    return this;
  }

  /**
   * Export to DOCX
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toDocx() throws DRException {
    JasperDocxExporterBuilder docxExporterBuilder = export.docxExporter(getTemplatePath().replace(EXTENSION_PATTERN, "docx"))
      .setFramesAsNestedTables(true);
    reportBuilder.toDocx(docxExporterBuilder);
    return this;
  }

  /**
   * Export to API XLS
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toExcel() throws DRException {
    return toXlsx();
  }

  /**
   * Export to ODS
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toOds() throws DRException {
    JasperOdsExporterBuilder odsExporterBuilder = export.odsExporter(getTemplatePath().replace(EXTENSION_PATTERN, "ods"))
      .setFlexibleRowHeight(true);
    reportBuilder.toOds(odsExporterBuilder);
    return this;
  }

  /**
   * Export to PNG
   *
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder toPng() {
    return this;
  }

  /**
   * Export to RTF
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toRtf() throws DRException {
    JasperRtfExporterBuilder rtfExporterBuilder = export.rtfExporter(getTemplatePath().replace(EXTENSION_PATTERN, "rtf"));
    reportBuilder.toRtf(rtfExporterBuilder);
    return this;
  }

  /**
   * Export to TXT
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toText() throws DRException {
    JasperTextExporterBuilder textExporter = export.textExporter(getTemplatePath().replace(EXTENSION_PATTERN, "txt"));
    reportBuilder.toText(textExporter);
    return this;
  }

  /**
   * Export to XLS
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toXls() throws DRException {
    Map<String, String> patterns = new HashMap<>();
    patterns.put("java.util.date", "dd/MM/YYYY");

    JasperXlsExporterBuilder xlsExporter = export.xlsExporter(getTemplatePath().replace(EXTENSION_PATTERN, "xls"))
            .setDetectCellType(true)
            .setIgnorePageMargins(true)
            .setWhitePageBackground(false)
            .setRemoveEmptySpaceBetweenColumns(true)
            .setIgnoreGraphics(false)
            .setFormatPatternsMap(patterns);

    // Remove headers and footers
    reportBuilder.setIgnorePagination(true) // Used as flag to get cell type
            .setColumnTitleStyle(StyleTemplate.COLUMN_TITLE_STYLE)
            .setPageHeaderPrintWhenExpression(exp.value(Boolean.FALSE))
            .setPageFooterPrintWhenExpression(exp.value(Boolean.FALSE))
            .rebuild()
            .toXls(xlsExporter);
    return this;
  }

  /**
   * Export to XLSX
   *
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toXlsx() throws DRException {
    Map<String, String> patterns = new HashMap<>();
    patterns.put("java.util.date", "dd/MM/YYYY");

    JasperXlsxExporterBuilder xlsxExporter = export.xlsxExporter(getTemplatePath().replace(EXTENSION_PATTERN, "xlsx"))
        .setDetectCellType(true)
        .setIgnorePageMargins(true)
        .setWhitePageBackground(false)
        .setRemoveEmptySpaceBetweenColumns(true)
        .setIgnoreGraphics(false)
        .setFormatPatternsMap(patterns);

    // Remove headers and footers
    reportBuilder.setIgnorePagination(true) // Used as flag to get cell type
      .setColumnTitleStyle(StyleTemplate.COLUMN_TITLE_STYLE)
      .setPageHeaderPrintWhenExpression(exp.value(Boolean.FALSE))
      .setPageFooterPrintWhenExpression(exp.value(Boolean.FALSE))
      .rebuild()
      .toXlsx(xlsxExporter);
    return this;
  }

  /**
   * Show generated report
   *
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder show() {
    try {
      this.reportBuilder
        .show();
    } catch (DRException exc) {
      log.error("Error showing output", exc);
    }
    return this;
  }

  /**
   * Show generated JRXML
   *
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder showJrxml() {
    try {
      this.reportBuilder
        .showJrXml();
    } catch (DRException exc) {
      log.error("Error showing JRXML", exc);
    }
    return this;
  }

  /**
   * Export to PDF Stream
   *
   * @param outputStream outputStream
   * @return TemplateExporterBuilderService
   * @throws DRException DRException exception
   */
  public TemplateExporterBuilder toPDFStream(OutputStream outputStream) throws DRException {
    reportBuilder.setDataSource(getDataSource());
    reportBuilder.toPdf(outputStream);
    return this;
  }
}
