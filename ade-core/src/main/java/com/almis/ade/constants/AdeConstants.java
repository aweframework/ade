package com.almis.ade.constants;

public class AdeConstants {

  public static final String ICON_PATH = "fonts/fontawesome";
  public static final String ICON_EXTENSION = ".svg";
}
