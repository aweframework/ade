
# Changelog for ADE 2.2.5
*23/10/2024*

- Exclude jackson-dataformat-xml. [MR #21](https://gitlab.com/aweframework/ade/-/merge_requests/21) (Pablo Javier García Mora)

# Changelog for ADE 2.2.4
*18/10/2024*

- Overwrite jasperreports dependency of dynamicjasper. [MR #20](https://gitlab.com/aweframework/ade/-/merge_requests/20) (Pablo Vidal Otero)
- Exclude jackson-dataformat-xml. [MR #19](https://gitlab.com/aweframework/ade/-/merge_requests/19) (Pablo Vidal Otero)
- Update spring boot to 3.4.x. [MR #18](https://gitlab.com/aweframework/ade/-/merge_requests/18) (Pablo Vidal Otero)
- Update jfreecharts vulnerability version. [MR #17](https://gitlab.com/aweframework/ade/-/merge_requests/17) (Pablo Vidal Otero)

# Changelog for ADE 2.2.3
*08/10/2024*

# Changelog for ADE 2.2.2
*04/10/2024*

# Changelog for ADE 2.2.1
*04/10/2024*

# Changelog for ADE 2.2.0
*31/08/2023*

- Support JDK 17 and spring boot 3. [MR #16](https://gitlab.com/aweframework/ade/-/merge_requests/16) (Pablo Vidal Otero)

# Changelog for ADE 2.1.6
*01/06/2023*

- Add getters and setters in ReportExporterBuilder. [MR #15](https://gitlab.com/aweframework/ade/-/merge_requests/15) (Pablo Vidal Otero)

# Changelog for ADE 2.1.5
*10/03/2023*

- Problem with ReportExporterBuilder. [MR #14](https://gitlab.com/aweframework/ade/-/merge_requests/14) (Pablo Vidal Otero)

# Changelog for ADE 2.1.4
*22/12/2022*

- Add PPTX exporter. [MR #13](https://gitlab.com/aweframework/ade/-/merge_requests/13) (Pablo Vidal Otero)
- Throw AWE exception in report generation. [MR #12](https://gitlab.com/aweframework/ade/-/merge_requests/12) (Pablo Vidal Otero)

# Changelog for ADE 2.1.3
*18/08/2022*

- Check for already compiled templates is not checking the right attribute. [MR #11](https://gitlab.com/aweframework/ade/-/merge_requests/11) (Pablo Javier García Mora)

# Changelog for ADE 2.1.2
*04/07/2022*

- Provide API to generate the report without having to compile. [MR #10](https://gitlab.com/aweframework/ade/-/merge_requests/10) (Pablo Vidal Otero)

# Changelog for ADE 2.1.1
*21/06/2022*

- Upgrade dependencies. [MR #9](https://gitlab.com/aweframework/ade/-/merge_requests/9) (Pablo Vidal Otero)

# Changelog for ADE 2.1.0
*25/01/2022*

- Remove log4j vulnerable libraries and update batik version. [MR #8](https://gitlab.com/aweframework/ade/-/merge_requests/8) (Pablo Javier García Mora)

# Changelog for ADE 2.0.18
*25/01/2022*

- Migrate Log4j2 logging system to Slf4j. [MR #7](https://gitlab.com/aweframework/ade/-/merge_requests/7) (Pablo Vidal Otero)

# Changelog for ADE 2.0.17
*08/06/2021*

- Problem with itext dependency. [MR #5](https://gitlab.com/aweframework/ade/-/merge_requests/5) (Pablo Vidal Otero)

# Changelog for ADE 2.0.16
*07/06/2021*

- Update dynamic and jasperreports libraries. [MR #4](https://gitlab.com/aweframework/ade/-/merge_requests/4) (Pablo Vidal Otero)

# Changelog for ADE 2.0.15
*24/03/2021*

# Changelog for ADE 2.0.14
*17/06/2020*

- Row styles are not being rendered right. [MR #2](https://gitlab.com/aweframework/ade/-/merge_requests/2) (Pablo Javier García Mora)

# Changelog for ADE 2.0.10
*05/06/2020*

- Bug with numeric columns. [MR #1](https://gitlab.com/api/v4/projects/10014452/merge_requests/1) (Pablo Javier García Mora)
