package com.almis.ade.autoconfigure;

import com.almis.ade.api.ADE;
import com.almis.ade.config.AdeConfigProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Ade autoconfiguration starter class
 */
@EnableConfigurationProperties(AdeConfigProperties.class)
@Configuration
public class AdeAutoConfiguration {

  /**
   * ADE API
   * @param adeConfigProperties ADE configuration properties
   * @return ADE bean api
   */
  @Bean
  @ConditionalOnMissingBean
  public ADE ade(AdeConfigProperties adeConfigProperties) {
    return new ADE(adeConfigProperties);
  }
}
